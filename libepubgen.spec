%global apiversion 0.1

Name: libepubgen
Version: 0.1.1
Release: 1
Summary: An EPUB generator library

License: MPL-2.0
URL: https://sourceforge.net/projects/libepubgen/
Source: http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.xz

BuildRequires: boost-devel doxygen gcc-c++ pkgconfig(cppunit) pkgconfig(librevenge-0.0) libxml2-devel make

%description
%{name} is a library for generating EPUB documents. It is directly
pluggable into import filters based on librevenge.

%package devel
Summary: Development files for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package_help

%prep
%setup -q

%build
%configure --disable-silent-rules --disable-static
sed -i \
    -e 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' \
    -e 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' \
    libtool
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot} INSTALL='install -p'
rm -f %{buildroot}/%{_libdir}/*.la
# we install API docs directly from build
rm -rf %{buildroot}/%{_docdir}/%{name}

%check
export LD_LIBRARY_PATH=%{buildroot}%{_libdir}${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
make %{?_smp_mflags} check

%ldconfig_scriptlets

%files
%doc AUTHORS README NEWS
%license COPYING
%{_libdir}/%{name}-%{apiversion}.so.*

%files devel
%doc ChangeLog
%{_includedir}/%{name}-%{apiversion}
%{_libdir}/%{name}-%{apiversion}.so
%{_libdir}/pkgconfig/%{name}-%{apiversion}.pc

%files help
%license COPYING
%doc docs/doxygen/html

%changelog
* Wed Sep 06 2023 Darssin <2020303249@mail.nwpu.edu.cn> - 0.1.1-1
- Package init
